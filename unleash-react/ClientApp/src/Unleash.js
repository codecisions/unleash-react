import { UnleashClient } from 'unleash-proxy-client';

const unleash = new UnleashClient({
    url: 'http://10.0.2.12:3000/proxy',
    clientKey: 'some-secret',
    appName: 'unleash-react',
  });

unleash.start();

export default unleash;